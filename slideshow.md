<div style="background: blue; color: white; text-align: center; padding: 5px;">
  <h1>Koty, kociaki, koteły</h1>
</div>
<h4 align="center">Przemysław Rogoś</h4>
<div style="page-break-after: always; break-after: page;"></div>
<div style="background: blue; color: white; text-align: center; padding: 5px;">
  <h2>Wstęp</h2>
</div>
Każdego dnia budzimy się z mruczeniem przy uchu, zasypiamy ze słodkim ciężarem futerka, a przy pracy towarzyszy nam dziwne ciepło na kolanach:
> Po prostu - masz kota! 

**„Koty dyktują ludziom i nigdy odwrotnie"** - *Ja*
<div style="page-break-after: always; break-after: page;"></div>

<div style="background: blue; color: white; text-align: center; padding: 5px;">
  <h2>Rasy kotów</h2>
</div>

1. Koty Syberyjskie
2. Koty Perskie
3. Koty Maine Coon

*Więcej ras mruczków:*

* Koty Syjamskie
* Koty Ragdoll
* Koty Brytyjskie
<div style="page-break-after: always; break-after: page;"></div>

<div style="background: blue; color: white; text-align: center; padding: 5px;">
  <h2>Kocie ubranka, czyli sierść</h2>
</div>
* Rasy długowłose
    * Rasy z futrem średnio-długim (np. Ragdoll)
    * Rasy z futrem długim (np. Perski)
* Rasy krótkowłose  
<div style="page-break-after: always; break-after: page;"></div>

<div style="background: blue; color: white; text-align: center; padding: 5px;">
  <h2>Slajd nr 4</h2>
</div>
Obliczenie jak wskoczyć na półkę, na którą człowiek nie pozwala wskakiwać

$$
L=\frac{d^2\Phi}{dA\cdot\cos\theta d\Omega}
$$
<div style="page-break-after: always; break-after: page;"></div>

<div style="background: blue; color: white; text-align: center; padding: 5px;">
  <h2>Slajd nr 5</h2>
</div>
Python code:
```python
# Prints out the numbers 0,1,2,3,4
for x in range(5):
    print(x)

# Prints out 3,4,5
for x in range(3, 6):
    print(x)

# Prints out 3,5,7
for x in range(3, 8, 2):
    print(x)
```
<div style="page-break-after: always; break-after: page;"></div>

<div style="background: blue; color: white; text-align: center; padding: 5px;">
  <h2>Koteł</h2>
</div> 
![obrazek](kot syberyjski.jpg)
<div style="page-break-after: always; break-after: page;"></div>

<div style="background: blue; color: white; text-align: center; padding: 5px;">
  <h2>Rasy kotów w tabelce</h2>
</div>
<table>
  <tr>
    <td colspan="2" style="text-align: center; background: pink">
      <h4>Takie koty</h4>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <ul>
        <li>Syberyjskie</li>
        <li>Perskie</li>
        <li>Maine Coon</li>
      </ul>
    </td>
    <td>Syjamskie</td>
  </tr>
  <tr>
    <td>Ragdoll</td>
  </tr>
  <tr>
    <td>Brytyjskie</td>
  </tr>
</table>